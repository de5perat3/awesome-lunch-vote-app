export const lunchPlaceKey = 'lunchPlaces'
export const archiveKey = 'archiveResults'

export const defaultPlaces = [
  {
    id: '35124125',
    name: 'My place',
    address: 'Fedkovucha 1',
    menu: 'link-1',
    description: 'Nice borw4'
  },
  {
    id: '351241256',
    name: '4 Places',
    address: 'Heroiv Upa 33',
    menu: 'menu - 1',
    description: 'Cool'
  },
  {
    id: '351241257',
    name: 'Cafe',
    address: 'Fedkovucha 1',
    menu: 'coffe menu',
    description: 'Great coffe'
  }
]
