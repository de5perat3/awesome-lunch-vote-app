const generateRandomDuration = (start = 0, end = 1) => Math.floor(Math.random() * end) + start

export const delay = () => new Promise((resolve) => {
  const randomDelayMilliseconds = generateRandomDuration(1, 2) * 1000

  setTimeout(() => { resolve() }, randomDelayMilliseconds)
})
