import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import TrashIcon from './components/icons/Trash.vue'
import ChevronRightIcon from './components/icons/ChevronRight.vue'
import DashboardIcon from './components/icons/Dashboard.vue'
import PlaceIcon from './components/icons/Place.vue'
import AnalyticsIcon from './components/icons/Analytics.vue'

Vue.component('TrashIcon', TrashIcon)
Vue.component('ChevronRightIcon', ChevronRightIcon)
Vue.component('DashboardIcon', DashboardIcon)
Vue.component('PlaceIcon', PlaceIcon)
Vue.component('AnalyticsIcon', AnalyticsIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
