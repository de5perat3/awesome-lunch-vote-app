export const getVoteSum = {
  methods: {
    getVoteSum (items = []) {
      return items.reduce((acc, { votes = 0 }) => (acc += votes), 0)
    }
  }
}
