import { delay } from '@/helpers'
import { lunchPlaceKey, archiveKey, defaultPlaces } from '@/helpers/constants'

class LunchPlace {
  async updatePlaces (places) {
    return localStorage.setItem(lunchPlaceKey, JSON.stringify(places))
  }

  async add (place) {
    const places = await this.getAll()
    const id = `${Date.now()}`
    const lunchPlaces = places.concat({ id, ...place })

    await this.updatePlaces(lunchPlaces)

    return { id }
  }

  async remove (placeID) {
    const places = await this.getAll()
    const lunchPlaces = places.filter(item => item.id !== placeID)

    await this.updatePlaces(lunchPlaces)

    return lunchPlaces
  }

  async getAll () {
    await delay()

    const lunchPlaces = localStorage.getItem('lunchPlaces')

    return lunchPlaces ? JSON.parse(lunchPlaces) : defaultPlaces
  }

  async updatePlaceVotes (placeID, voteValue) {
    const places = await this.getAll()
    const votedPlace = places.find(item => item.id === placeID)
    const newVoteValue = voteValue > 0 ? 1 : -1
    const updatedVotes = votedPlace.votes ? votedPlace.votes + (newVoteValue) : 0 + newVoteValue
    const increasedVote = { ...votedPlace, votes: updatedVotes }

    const updatedPlacesVotes = places.map(item => {
      return item.id === placeID ? increasedVote : item
    })

    return this.updatePlaces(updatedPlacesVotes)
  }

  async resetVotes () {
    const places = await this.getAll()

    return this.updatePlaces(places.map(item => ({ ...item, votes: 0 })))
  }
}

class Archive {
  async getAll () {
    await delay()

    const voteResults = localStorage.getItem(archiveKey)

    return voteResults ? JSON.parse(voteResults) : []
  }

  async update (results) {
    const id = `${Date.now()}`
    const archiveDate = new Date()
    const archiveResults = await this.getAll()
    const maxVotes = results.reduce((prev, current) => prev.votes > current.votes ? prev : current)
    const data = results.map(item => {
      return item.id === maxVotes.id ? { ...item, selected: true } : item
    })
    const finalResults = archiveResults.concat({ id, data, archiveDate })

    return localStorage.setItem(archiveKey, JSON.stringify(finalResults))
  }
}

export const LunchPlaceModel = LunchPlace
export const ArchiveModel = Archive
