export default class ArchiveService {
  constructor (model) {
    this.model = model
  }

  getArchive () {
    return this.model.getAll()
  }

  updateArchive (results) {
    return this.model.update(results)
  }
}
