export default class LunchVoteService {
  constructor (model) {
    this.model = model
  }

  getAllLunchPlace () {
    return this.model.getAll()
  }

  addLunchPlace (place) {
    return this.model.add(place)
  }

  removeLunchPlace (placeID) {
    return this.model.remove(placeID)
  }

  voteForLunchPlace (data, value) {
    return this.model.updatePlaceVotes(data, value)
  }

  getVoteResults (data) {
    return this.model.getVoteResults(data)
  }

  resetVoteResults () {
    return this.model.resetVotes()
  }
}
