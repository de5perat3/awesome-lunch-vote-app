import Lunch from './LunchService'
import Archive from './ArchiveService'
import { LocalStorageModel } from '@/models'

export const LunchService = new Lunch(new LocalStorageModel.LunchPlaceModel())
export const ArchiveService = new Archive(new LocalStorageModel.ArchiveModel())
