import { LunchService, ArchiveService } from '@/services'

export default {

  async getLunchPlaces ({ commit }) {
    try {
      const data = await LunchService.getAllLunchPlace()
      commit('setLunchPlaces', data)
    } catch (error) {
      console.error('Error getLunchPlaces:', error)
    }
  },

  async submitLunchPlace ({ commit }, place) {
    try {
      const { id } = await LunchService.addLunchPlace(place)
      commit('pushLunchPlace', { id, ...place })
    } catch (error) {
      console.error('Error pushLunchPlace:', error)
    }
  },

  async removeLunchPlace ({ commit }, placeID) {
    try {
      const data = await LunchService.removeLunchPlace(placeID)
      commit('setLunchPlaces', data)
    } catch (error) {
      console.error('Error removeLunchPlace:', error)
    }
  },

  async voteForLunchPlace ({ dispatch }, data) {
    const { placeID, value } = data

    try {
      await LunchService.voteForLunchPlace(placeID, value)

      await dispatch('getLunchPlaces')
    } catch (error) {
      console.error('Error voteForLunchPlace:', error)
    }
  },

  finalizeVotes ({ commit }, places) {
    return ArchiveService.updateArchive(places)
  },

  resetVotes () {
    return LunchService.resetVoteResults()
  },

  async getVoteStatistics ({ commit }) {
    try {
      const data = await ArchiveService.getArchive()
      commit('setVoteStatistics', data)
    } catch (error) {
      console.error('Error getVoteStatistics:', error)
    }
  }
}
