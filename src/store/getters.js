export default {
  lunchPlaces: state => state.lunchPlaces,
  voteStatistics: state => state.voteStatistics
}
