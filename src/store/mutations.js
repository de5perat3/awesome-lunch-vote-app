export default {
  pushLunchPlace (state, place) {
    state.lunchPlaces = state.lunchPlaces.concat(place)
  },

  setLunchPlaces (state, places) {
    state.lunchPlaces = places
  },

  setVoteStatistics (state, statistics) {
    state.voteStatistics = statistics
  }
}
