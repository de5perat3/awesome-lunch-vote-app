import { mount } from '@vue/test-utils'
import AddressForm from '@/components/AddressForm.vue'

describe('AddressForm.vue', () => {
  it('should validate form and set errors', async () => {
    const wrapper = mount(AddressForm, {})

    wrapper.vm.submitLunchPlace = jest.fn()

    const cancelButton = wrapper.findAllComponents({ name: 'VueButton' }).at(0)
    await cancelButton.trigger('click')

    expect(wrapper.vm.errors.address).toEqual('address is required')
  })

  it('should validate submitForm', async () => {
    const wrapper = mount(AddressForm, {})

    wrapper.vm.submitLunchPlace = jest.fn()
    wrapper.vm.clearForm = jest.fn()

    const formData = {
      name: 'name',
      description: 'desc',
      address: 'addr',
      menu: 'menu'
    }

    await wrapper.setData({ formData })

    const cancelButton = wrapper.findAllComponents({ name: 'VueButton' }).at(0)
    await cancelButton.trigger('click')

    expect(wrapper.vm.submitLunchPlace).toHaveBeenCalledWith(formData)
    expect(wrapper.vm.clearForm).toHaveBeenCalled()
  })
})
