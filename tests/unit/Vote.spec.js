import { shallowMount, mount } from '@vue/test-utils'
import Vote from '@/components/Vote.vue'

describe('VoteResults.vue', () => {
  const votes = [
    {
      id: 'id-1',
      votes: 1,
      percent: '25%',
      name: 'place-1'
    },
    {
      id: 'id-2',
      votes: 3,
      percent: '75%',
      name: 'place-2'
    }
  ]

  it('should render RadioButtonGroup component', () => {
    const wrapper = shallowMount(Vote, {
      propsData: { items: votes }
    })
    const radioButtonGroup = wrapper.findComponent({ name: 'RadioButtonGroup' })

    expect(radioButtonGroup.exists()).toBeTruthy()
  })

  it('should render VoteResults component', async () => {
    const wrapper = mount(Vote, {
      propsData: { items: votes }
    })

    await wrapper.setData({ votingResults: true, isVoting: false, selectedVote: votes[0] })

    const voteResults = wrapper.findComponent({ name: 'VoteResults' })

    expect(voteResults.exists()).toBeTruthy()
  })

  it('should handle vote and setVotingResults as true', async () => {
    const wrapper = mount(Vote, {
      propsData: { items: votes }
    })

    wrapper.vm.voteForLunchPlace = jest.fn()

    await wrapper.setData({ selectedVote: votes[0] })

    const voteButton = wrapper.findAllComponents({ name: 'VueButton' }).at(0)
    await voteButton.trigger('click')
    expect(wrapper.vm.votingResults).toBeTruthy()
  })

  it('should cancel vote clear selectedVote', async () => {
    const wrapper = mount(Vote, {
      propsData: { items: votes }
    })

    wrapper.vm.voteForLunchPlace = jest.fn()

    await wrapper.setData({ votingResults: true, isVoting: false, selectedVote: votes[0] })

    const cancelButton = wrapper.findAllComponents({ name: 'VueButton' }).at(0)
    await cancelButton.trigger('click')

    expect(wrapper.vm.isVoting).toBeTruthy()
    expect(wrapper.vm.selectedVote).toBeFalsy()
  })

  it('should display vote final', async () => {
    const wrapper = mount(Vote, {
      propsData: { items: votes }
    })

    wrapper.vm.finalizeVotes = jest.fn()
    wrapper.vm.resetVotes = jest.fn()

    await wrapper.vm.finishVoting()

    expect(wrapper.vm.voteFinal).toEqual({
      action: 'Start new Vote',
      result: 'place-2',
      text: 'Have a great meal at'
    })
  })

  it('should display re-vote text in case equalVotes', async () => {
    const equalVotes = [
      { ...votes[0] },
      {
        id: 'id-2',
        votes: 1,
        percent: '25%',
        name: 'place-2'
      }
    ]
    const wrapper = mount(Vote, {
      propsData: { items: equalVotes }
    })

    wrapper.vm.finalizeVotes = jest.fn()
    wrapper.vm.resetVotes = jest.fn()

    await wrapper.vm.finishVoting()

    expect(wrapper.vm.voteFinal).toEqual({
      action: 'Re-vote',
      result: 'place-1, place-2',
      text: 'Please vote again, these places have the same amount of votes'
    })
  })
})
