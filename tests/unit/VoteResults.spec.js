import { shallowMount } from '@vue/test-utils'
import VoteResults from '@/components/VoteResults.vue'

describe('VoteResults.vue', () => {
  const votes = [
    {
      id: 'id-1',
      votes: 1,
      percent: '25%'
    },
    {
      id: 'id-2',
      votes: 3,
      percent: '75%'
    }
  ]

  it('renders with percent value', () => {
    const wrapper = shallowMount(VoteResults, {
      propsData: { votes }
    })
    const percent = wrapper.findAll('.percent').at(0)

    expect(percent.text()).toEqual('25%')
  })

  it('renders with selected class', () => {
    const wrapper = shallowMount(VoteResults, {
      propsData: { votes, selected: 'id-2' }
    })
    const selected = wrapper.get('.selected')

    expect(selected.exists()).toBeTruthy()
  })
})
